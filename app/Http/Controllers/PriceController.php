<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PriceController extends Controller
{
    /**
     * @var array
     */
    protected $cart = [];

    /**
     * @var int[]
     */
    protected $pricing = [
        'A' => 50,
        'B' => 30,
        'C' => 20,
        'D' => 15,
        'E' => 5
    ];

    /**
     * @var int[][]
     */
    protected $discounts = [
        'A' => [
            'threshold' => 3,
            'splamount' => 130
        ],
        'B' => [
            'threshold' => 2,
            'splamount' => 45
        ],
    ];

    /**
     * @var int[][]
     */
    protected $discountsmultiple = [
        'C' => [
                    [
                        'threshold' => 2,
                        'splamount' => 38
                    ],
                    [
                        'threshold' => 5,
                        'splamount' => 50
                    ]
                ],
    ];

    /**
     * @var int[][]
     */
    protected $discountsbyproduct = [
        'D' => [
            'threshold' => 'A',
            'splamount' => 5
        ],
    ];



    /**
     * @var int[]
     */
    protected $stats = [
        'A' => 0,
        'B' => 0,
        'C' => 0,
        'D' => 0,
    ];

    /**
     * Adds an item to the checkout
     *
     * @param $sku string
     */
    public function scan(string $sku)
    {
        if (!array_key_exists($sku, $this->pricing)) {
            return;
        }
        if( $sku != "E") {
          $this->stats[$sku] = $this->stats[$sku] + 1;
        }
        

        $this->cart[] = [
            'sku' => $sku,
            'price' => $this->pricing[$sku]
        ];
    }

    /**
     * Adds an item to the checkout
     *
     * @param $sku array
     */
    public function multiscan(array $skus)
    {
        foreach($skus as $sku=>$count){
            for($i=0;$i<$count;$i++){
                $this->scan($sku);
            }
        }
    }

    /**
     * Calculates the total price of all items in this checkout
     *
     * @return int
     */
    public function total(): int
   {
        $standardPrices = array_reduce($this->cart, function ($total, array $product) {
            $total += $product['price'];
            return $total;
        }) ?? 0;

        $totalDiscount = 0;

        foreach ($this->discounts as $key => $discount) {
            
            if ($this->stats[$key] >= $discount['threshold']) {
                $numberOfSets = floor($this->stats[$key] / $discount['threshold']);                
                $discount['amount'] = floor($this->pricing[$key]*$discount['threshold'])-floor($discount['splamount']);
                $totalDiscount += ($discount['amount'] * $numberOfSets);
            }
        }

        foreach ($this->discountsmultiple as $sku => $discounts) {
            $multistats  =  $this->stats[$sku];         
            $prices = array_column($discounts, 'threshold');
            array_multisort($prices, SORT_DESC, $discounts);
            foreach ($discounts as $key => $discount) {
                if ($multistats >= $discount['threshold']) {
                    $numberOfSets = floor($multistats / $discount['threshold']);                        
                    $multistats= floor($multistats % $discount['threshold']); 
                    $discount['amount'] = floor($this->pricing[$sku]*$discount['threshold'])-floor($discount['splamount']);
                    $totalDiscount += ($discount['amount'] * $numberOfSets);
                }
            }
        }

        foreach ($this->discountsbyproduct as $key => $discount) {
            if($this->stats[$discount['threshold']]==0){
                continue;
            }
            
            if($this->stats[$key]>=$this->stats[$discount['threshold']]){
                $numberOfSets = $this->stats[$discount['threshold']];
            }
            else{
                $numberOfSets = $this->stats[$key];
            }
            
            $discount['amount'] = (($this->pricing[$key]-$discount['splamount'])*$numberOfSets);

            
            $totalDiscount += $discount['amount'];

        }
        
        return $standardPrices - $totalDiscount;
    }

    /**
     * Function to calulcatePrice
     */
    public function calulcatePrice()
    {
        $this->multiscan(['A'=>3,'B'=>2]);
        echo "<br /> ['A'=>3,'B'=>2] Total price is : ".$this->total()."  ";
        $this->multiscan(['A'=>4,'D'=>3]);
        echo " <br />['A'=>4,'D'=>3] Total price is :".$this->total()."  ";
        $this->multiscan(['C'=>2]);
        echo "<br />['C'=>2 ] Total price is :". $this->total()."  ";
        $this->multiscan(['E'=>1]);
        echo "<br />['E'=>1 ] Total price is :". $this->total()."  ";
    }
}
